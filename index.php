<?php
$method = $_SERVER['REQUEST_METHOD'];
$formData = getFormData($method);
function getFormData($method) {

    if ($method === 'GET') return $_GET;
    if ($method === 'POST') return $_POST;
}

$url = (isset($_GET['q'])) ? $_GET['q'] : '';
$url = rtrim($url, '/');
$urls = explode('/', $url);
$router = $urls[0];
$urlData = array_slice($urls, 1);

include_once 'routers/' . $router . '.php';
route($method, $urlData, $formData);
?>